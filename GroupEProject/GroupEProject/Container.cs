﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupEProject
{
    class Container
    {
        private List <Item> items;
        public List <Item> Items { get { return items; } set { items = value; } }

        public Item GetItem(int i) { return items[i]; }

       public Container()
        {
            items = null;
        }

       public Container(List<Item> i)
        {
            items = i;
        }


    }
}

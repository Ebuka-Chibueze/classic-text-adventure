﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupEProject
{
    class Item
    {
        private string name;
        private string description;
        private int visible;
        private int carryable;

        public string Name { get { return name; } set { name = value; } }
        public string Description { get { return description; } set { description = value; } }
        public int  Visible { get { return visible; } set { visible = value; } }
        public int Carryable { get { return carryable; } set { carryable = value; } }

        public Item() { Name = ""; Description = ""; Visible = 0; Carryable = 0; }

        public Item(string n, string d, int v, int c) { Name = n; Description = d; Visible = v; Carryable = c; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupEProject
{
    class Actor : Container
    {
        private int currentRoom;
        private int health;

        public int CurrentRoom { get { return currentRoom; } set { currentRoom = value; } }
        public int Health { get { return health; } set { health = value; } }

        public Actor():base()
        {
            CurrentRoom = 0;
            Health = 0;
        }

        public Actor(int cr, int h, List<Item> i):base(i)
        {
            CurrentRoom = cr;
            Health = h;
        }

    }
}

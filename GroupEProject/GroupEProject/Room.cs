﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupEProject
{
    class Room : Container
    {
        private string name;
        private int roomNum;
        private string description;
        //private string person;
        int roomLeft, roomRight, roomFoward, roomBackward;
        int leftPossible,rightPossible,fowardPossible,backPossible;

        public string Name { get { return name; } set { name = value; } }
        public int RoomNum { get { return roomNum; } set { roomNum = value; } }
        public int RoomLeft { get { return roomLeft; } set { roomLeft = value; } }
        public int RoomRight { get { return roomRight; } set { roomRight = value; } }
        public int RoomFoward { get { return roomFoward; } set { roomFoward = value; } }
        public int RoomBackward { get { return roomBackward; } set { roomBackward = value; } }
        public string DescriptionRoom { get { return description; } set { description = value; } }
       // public string Person { get { return person; } set { person = value; } }

        public int LeftPossible { get { return leftPossible; } set { leftPossible = value; } }
        public int RightPossible { get { return rightPossible; } set { rightPossible = value; } }
        public int FowardPossible { get { return fowardPossible; } set { fowardPossible = value; } }
        public int BackPossible { get { return backPossible; } set { backPossible = value; } }
        

        public Room():base()
        {
            Name = "";
            RoomLeft = 0;
            RoomRight = 0;
            RoomFoward = 0;
            RoomBackward = 0;
            DescriptionRoom = "";
           // Person = "";
        }

        public Room(string nm, int rl, int rr, int rf, int rb, int lp, int rp, int fp, int bp,string dr, List<Item> i):base(i)
        {
            Name = nm;
            RoomLeft = rl;
            RoomRight = rr;
            RoomFoward = rf;
            RoomBackward = rb;
            DescriptionRoom = dr;
            LeftPossible = lp;
            RightPossible = rp;
            FowardPossible = fp;
            BackPossible = bp;
            Items = i;
           // Person = p;
        }
        


    }
}

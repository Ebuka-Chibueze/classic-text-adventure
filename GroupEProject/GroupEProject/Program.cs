﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GroupEProject
{
    class Program
    {
        static void Main()
        {
            int keys = 0;
            List<Item> inventory = new List<Item>();
            Actor You = new Actor(0, 5, inventory);
            You.CurrentRoom = 0;
            string havesandwich="";
            Console.ForegroundColor = ConsoleColor.White;
            int welcome = WelcomeScreen();
            List<Room> rooms = new List<Room>();
            Readfile(out rooms); Console.ForegroundColor = ConsoleColor.White;
            while (keys == 0)
            {
                Console.WriteLine("{0}", rooms[You.CurrentRoom].DescriptionRoom);//prints the description
                Console.WriteLine("In the room there is:");
                for (int i = 0; i < rooms[You.CurrentRoom].Items.Count; i++)//prints the items
                {
                    if (rooms[You.CurrentRoom].Items[i].Visible == 1)
                        Console.WriteLine("{0}", rooms[You.CurrentRoom].Items[i].Name.ToLower());
                }
                string[] valid = { "MOVE", "TAKE", "LOOK","DROP" , "TALK","INVENTORY","MENU","USE" };
                int a = 0;
                while (a == 0)
                {
                    breaker2:
                    string ans = GetString("What would you like to do? ", valid, "Please enter take, look, drop, use, move, talk, menu or inventory");//ask the user what they want to do then does it
                    if (ans == "MOVE")
                    {
                        break;
                    }
                    if (ans == "TAKE")
                    {
                        You.Items = Take(rooms[You.CurrentRoom], You);
                        for (int i = 0; i <= rooms[You.CurrentRoom].Items.Count - 1; i++)
                            if (rooms[You.CurrentRoom].Items[i].Name.ToUpper() == inventory[inventory.Count - 1].Name)
                            {
                                rooms[You.CurrentRoom].Items[i] = new Item();
                            }
                        Console.WriteLine("In your inventroy there is:");
                        for (int i = 0; i < inventory.Count; i++)
                            Console.WriteLine("{0}", inventory[i].Name);
                        for (int i = 0; i < inventory.Count; i++)
                        {
                            if (inventory[i].Name.ToUpper() == "BOOK")
                            {
                                for (int j = 0; j < rooms[You.CurrentRoom].Items.Count; j++)
                                {
                                    if (rooms[You.CurrentRoom].Items[j].Name.ToUpper() == "PAPERCLIP")
                                    {
                                        Console.WriteLine("When you pick up the book a paperclip falls out");
                                        rooms[You.CurrentRoom].Items[j].Visible = 1;
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < inventory.Count; i++)
                        {
                            if (inventory[i].Name.ToUpper() == "TOWEL")
                            {
                                for (int j = 0; j < rooms[You.CurrentRoom].Items.Count; j++)
                                {
                                    if (rooms[You.CurrentRoom].Items[j].Name.ToUpper() == "KEYS")
                                    {
                                        Console.WriteLine("When you pick up the towel the keys falls out");
                                        rooms[You.CurrentRoom].Items[j].Visible = 1;
                                    }
                                }
                            }
                        }
                        Console.WriteLine("In the room there is:");
                        for (int i = 0; i < rooms[You.CurrentRoom].Items.Count; i++)//prints the items
                        {
                            if (rooms[You.CurrentRoom].Items[i].Visible == 1)
                                Console.WriteLine("{0}", rooms[You.CurrentRoom].Items[i].Name.ToLower());
                        }
                    }
                       
                    if (ans == "MENU")
                    {
                       int menu = MenuScreen();
                        goto breaker2;
                    }
                    if (ans == "LOOK")
                    {
                        Look(rooms[You.CurrentRoom].Items);
                        goto breaker2;
                    }
                    if(ans=="DROP")
                    {
                        Console.WriteLine("In your inventroy there is:");
                        for (int i = 0; i < inventory.Count; i++)
                            Console.WriteLine("{0}", inventory[i].Name);
                        Console.WriteLine("What would you like to drop? ");
                        string dropped = Console.ReadLine().ToUpper();
                        for(int i=0;i<inventory.Count;i++)
                        {
                            if (dropped == inventory[i].Name)
                            {
                                rooms[You.CurrentRoom].Items.Add(inventory[i]);//add to room
                                inventory.RemoveAt(i);// remove from inventory
                            }
                        }
                        Console.WriteLine("In your inventroy there is:");
                        for (int i = 0; i < inventory.Count; i++)
                            Console.WriteLine("{0}", inventory[i].Name);
                        Console.WriteLine("In the room there is:");
                        for (int i = 0; i < rooms[You.CurrentRoom].Items.Count; i++)//prints the items
                        {
                            if (rooms[You.CurrentRoom].Items[i].Visible == 1)
                                Console.WriteLine("{0}", rooms[You.CurrentRoom].Items[i].Name.ToLower());
                        }

                    }
                    if(ans=="TALK")
                    {
                        for(int i=0;i<rooms[You.CurrentRoom].Items.Count;i++)
                        {
                            if (rooms[You.CurrentRoom].Items[i].Name.ToUpper() == "HOBO")
                            {
                                for(int x=0;x<inventory.Count;x++)
                                { 
                                    string y = inventory[x].Name;
                                    if (y == "SANDWICH")
                                    {
                                        havesandwich = y;
                                        Console.WriteLine("The hobo rambles on making no sense.");
                                        goto breaker2;
                                    }
                                  
                                }

                                Console.WriteLine("The hobo is sleeping.");
                                goto breaker2;
                            }
                            else
                            {
                                Console.WriteLine("There is no one here...");
                                goto breaker2;
                            }
                        }
                    }
                    if (ans == "INVENTORY")
                    {
                        for (int i = 0; i < inventory.Count; i++)
                            Console.WriteLine("{0}", inventory[i].Name);
                        if (inventory.Count == 0)
                        {
                            Console.WriteLine("You have no items...");
                            goto breaker2;
                        }
                    }
                    int t = 0;
                    if (ans == "USE")
                    {
                        for (int i = 0; i < inventory.Count; i++)
                        Console.WriteLine("{0}", inventory[i].Name);
                        Console.WriteLine("What would you like to use?");
                        string use = Convert.ToString(Console.ReadLine()).ToUpper();
                        if (use == "NONE")
                            goto breaker2;
                        if(inventory.Count == 0)
                        {
                            Console.WriteLine("You have no items...");
                            goto breaker2;
                        }
                        for (int q = 0; q < inventory.Count;q++ )
                        { 
                            if (use == "SANDWICH" && inventory[q].Name.ToUpper() == "SANDWICH" && You.CurrentRoom == 4)
                            {
                                havesandwich = "";
                                for (int i = 0; i < inventory.Count; i++)
                                {
                                    if (use == inventory[i].Name)
                                    {
                                        Console.WriteLine("You threw the sandwich at the hobo and he stopped talking.");
                                        inventory.RemoveAt(i);
                                    }
                                }
                                goto breaker2;
                            }
                        if (inventory[q].Name.ToUpper() == "PAPERCLIP" && You.CurrentRoom == 5 && use == "PAPERCLIP")
                        {
                            Console.WriteLine("You've unlocked the door.");
                            rooms[5].FowardPossible = 1;
                            goto breaker2;
                        }

                        if(inventory[q].Name.ToUpper() == "KNIFE" && use == "KNIFE" && You.CurrentRoom == 4)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("\nThe very agile hobo catches the knife and returns the favor\n\n...you are now dead.\n");
                            Playagain();
                            goto breaker;
                        }
                            if(use==inventory[q].Name.ToUpper())
                            {
                                t++;
                            }
                        }
                        if (t == 0)
                        {
                            Console.WriteLine("You do not have that item.");
                        }
                        else
                        {
                            Console.WriteLine("You cannot use that here.");
                            goto breaker2;
                        }

                    }
                    if (inventory.Count != 0)
                    {
                        if (inventory[inventory.Count - 1].Name.ToUpper() == "KEYS")
                        {
                            Win();
                            Playagain();
                            goto breaker;
                            keys = 1;
                        }
                    }
                }
                if (havesandwich != "SANDWICH")
                    You.CurrentRoom = Move(rooms[You.CurrentRoom], You);//moves the user to a new room
                else
                    Console.WriteLine("\n\nYou cannot move the hobo is takling to you.\n\n");

            }
        breaker:
            Console.WriteLine("Thanks for playing...");
            Console.Read();
        }

      
         public static int WelcomeScreen()
        {
            Console.WriteLine("Welcome to The House!\n\nYou wake up on the floor,\nthe sun is shining in your face through the window of your entry. \nYou are still wearing yesterday’s clothes. \nIn a panic you look over at the clock.\n\nIT'S 10:01 AM.\n\nYou're an hour late for work!!! \n\nYou frantically search through your pockets for your car keys, nothing. \nThey must be around here somewhere.\nHopefully you can find them before the boss notices you’re missing!\n\nPress Enter to start your search...");
            Console.ReadLine();
            Console.Clear();
            return 0;
        }
        public static int MenuScreen()
        {
            Console.WriteLine("\nMENU\nThe objective of the game is to find your car keys.\nTo move around in the game type north, south east, or west.\nEach direction will take you to a new room.\nKeywords include: take, look, drop, use, move, talk, menu and inventory\nOnly one word responses will be allowed.\nIf you are killed it is game over.\n\n");
            return 0;
        }
        static void Readfile(out List<Room> r)//reads whats in the file
        {
            string line;
            string[] tokens;
            string[] tokens2;
            string roomName;
            int roomNum;
            string roomDes;
            int c=0;
            r = new List<Room>();
            try
            {
                using (StreamReader sr = new StreamReader(@"..\..\Direction.txt"))
                {
                    while (sr.Peek() >= 0)
                    {
                        line = sr.ReadLine();
                        tokens = line.Split(' ');
                        roomName = tokens[0];
                        roomNum = Convert.ToInt32(tokens[1]);
                        tokens2 = line.Split('-');
                        roomDes = tokens2[1];
                        int i = 0;
                        Item r1 = new Item();
                        List<Item> tempItem=new List<Item> ();
                        line = sr.ReadLine();
                        while(line!="@")//reads all the items
                        {
                            tokens=line.Split(' ');
                            tokens2=line.Split('-');
                            r1.Name = tokens[0];
                            r1.Description = tokens2[1];
                            r1.Visible = Convert.ToInt32(tokens[1]);
                            r1.Carryable = Convert.ToInt32(tokens[2]);
                            tempItem.Add(new Item(r1.Name,r1.Description,r1.Visible,r1.Carryable));
                            line = sr.ReadLine();
                            i++;
                        }
                        Room temp = new Room();
                        line = sr.ReadLine();
                        while(line!="~")//reads the directions
                        {
                            tokens=line.Split(' ');
                            temp.FowardPossible=Convert.ToInt32(tokens[1]);
                            temp.RoomFoward=Convert.ToInt32(tokens[2]);
                            line=sr.ReadLine();
                            tokens=line.Split(' ');
                            temp.BackPossible=Convert.ToInt32(tokens[1]);
                            temp.RoomBackward=Convert.ToInt32(tokens[2]);
                            line=sr.ReadLine();
                            tokens=line.Split(' ');
                            temp.LeftPossible=Convert.ToInt32(tokens[1]);
                            temp.RoomLeft=Convert.ToInt32(tokens[2]);
                            line=sr.ReadLine();
                            tokens=line.Split(' ');
                            temp.RightPossible=Convert.ToInt32(tokens[1]);
                            temp.RoomRight=Convert.ToInt32(tokens[2]);
                            line = sr.ReadLine();
                        }
                        r.Add(new Room(roomName,temp.RoomLeft,temp.RoomRight,temp.RoomFoward,temp.RoomBackward,temp.LeftPossible,temp.RightPossible,temp.FowardPossible,temp.BackPossible,roomDes,tempItem));
                        r[c].Name = roomName;
                        r[c].RoomNum = roomNum;
                        c++;
                    }
                    sr.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Can't open file because: {0}", e.Message);
            }

        }
        public static int Move(Room r,Actor you)//ask the user what direction they want to go in and changes they current room
        {
            int directionEntered=0;
            string[] valid ={"NORTH","SOUTH","WEST","EAST","NONE"};
            while (directionEntered == 0)
            {
                string ans = GetString("What direction would you like to go in? ", valid, "Please enter north, south, east, west or none.");
                if (ans == "NORTH")
                {
                    if (r.FowardPossible == 1)
                    {
                        you.CurrentRoom = r.RoomFoward;
                        directionEntered = 1;
                    }
                    else if (you.CurrentRoom == 5 && r.FowardPossible == 0)
                    {
                        Console.WriteLine("\nThe bathroom door is locked it requires something small to pick open.\n");
                        break;
                    }
                    else
                        Console.WriteLine("You cannot travel in that direction.");
                }
                if (ans == "SOUTH")
                {
                    if (r.BackPossible == 1)
                    {
                        you.CurrentRoom = r.RoomBackward;
                        directionEntered = 1;
                    }
                    else
                        Console.WriteLine("You cannot travel in that direction.");
                }
                if (ans == "WEST")
                {
                    if (r.LeftPossible == 1)
                    {
                        you.CurrentRoom = r.RoomLeft;
                        directionEntered = 1;
                    }
                    else
                        Console.WriteLine("You cannot travel in that direction.");
                }
                if (ans == "EAST")
                {
                    if (r.RightPossible == 1)
                    {
                        you.CurrentRoom = r.RoomRight;
                        directionEntered = 1;
                    }
                    else
                        Console.WriteLine("You cannot travel in that direction.");
                }

                if (ans == "NONE")
                    break;
            }
            return you.CurrentRoom;
        }
        static string GetString(string prompt, string[] valid, string error)
        {
            string response;
            bool ok = false;
            do
            {
                Console.Write(prompt);
                response = Console.ReadLine().ToUpper();
                foreach (string s in valid) if (response == s.ToUpper()) ok = true;
                if (!ok) Console.WriteLine(error);
            } while (!ok);
            return response;
        }
        static List<Item> Take(Room r,Actor you)
        {
                string[] valid2={"NOTHING"," "," "," "," "," "};
                for (int index = 1; index <= r.Items.Count; index++)
                {
                    if(r.Items[index-1].Visible==1)
                    valid2[index] = r.Items[index-1].Name.ToUpper();
                }
                int a=0;
                while (a == 0)
                {
                    string i = GetString("Take what? ", valid2, "Please enter a item in the room");
                    for (int t = 0; t < r.Items.Count; t++)
                    {
                        if (i == r.Items[t].Name.ToUpper())
                        {
                            if (r.Items[t].Carryable == 1)
                            {
                                you.Items.Add(new Item(i, r.Items[t].Description, r.Items[t].Visible, r.Items[t].Carryable));
                                a++;
                            }
                            else
                                Console.WriteLine("You cannot carry the {0}", r.Items[t].Name);
                        }
                        if (i == "NOTHING")
                            a++;
                    }
                }
                return you.Items;
        }
        static void Look(List<Item> Items)
        {
            string[] valid2 = { "NOTHING", " ", " ", " ", " ", " " };
            for (int index = 1; index <= Items.Count; index++)
            {
                if (Items[index - 1].Visible == 1)
                    valid2[index] = Items[index - 1].Name.ToUpper();
            }
            string ans = GetString("Look at what? ", valid2, "Please enter an item in the room");
            int a = 0;
            while (a == 0)
            {
                for (int t = 0; t < Items.Count; t++)
                {
                    if (ans == Items[t].Name.ToUpper())
                    {
                        Console.WriteLine("{0}", Items[t].Description);
                        a++;
                    }
                }

                if (ans == "NOTHING")
                {
                    a++;
                }
            }
        }
        static void Win()
        {
            Console.WriteLine("You have found your car keys.\nYou Won! Now go to work...");
        }
        static void Playagain()
        {
            Console.ForegroundColor = ConsoleColor.White;
            string[] valid={"YES","NO"};
            string ans = GetString("Do you want to play again? ", valid, "Please enter yes or no");
            if (ans == "YES")
            {
                Console.Clear();
                Main();
            }
            if(ans=="NO")
            {
                
            }
        }
    }
}